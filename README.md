Clone All Project

# Codebase Backend

Use a different CMD from the Codebase Frontend.

## Installation
cd `Backend`

Run `npm install`

Run `npm install -g nodemon`

Run `nodemon`


# Codebase Frontend

## Installation
cd `Frontend`

Run `npm install`

Run `npm run dev`

Open `localhost:5173` in browser

## Development

Run `npm run dev`

Open `localhost:5173` in browser


