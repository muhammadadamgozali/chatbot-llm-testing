import PropTypes from 'prop-types';
import Chat from "../../components/Chat/Chat";

const Dashboard = ({ onMessageSubmit }) => {
    return (
        <div>
        <Chat onMessageSubmit={onMessageSubmit} />
        </div>
    )
}
Dashboard.propTypes = {
    onMessageSubmit: PropTypes.func.isRequired, // Prop harus berupa sebuah fungsi yang diperlukan
  };
export default Dashboard