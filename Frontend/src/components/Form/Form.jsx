import { useState } from "react";
import { useNavigate } from "react-router-dom";
import "./Form.css";
import { useAppContext, useNextButtonContext } from '../context/Context';

function Form() {
  const [isChecked, setIsChecked] = useState(false);
  const [alertMessage, setAlertMessage] = useState("");
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const { updateFormData } = useAppContext();
  const { setIsNextClicked, setIsAuthenticated } = useNextButtonContext(); 
  const navigate = useNavigate();
  


  const handleCheckboxChange = (event) => {
    setIsChecked(event.target.checked);
    setAlertMessage("")
  };

  const handleNextButtonClick = async (e) => {
   const isDataExists = await checkDataExists(name, email);
    if (!name.trim() || !email.trim()) {
      e.preventDefault();
      alert("Fill in your name and email.");
    } else if (!isChecked) {
      e.preventDefault();
      alert("Click the checkbox first.");
    } else if (email && !email.endsWith("@telkom.co.id")) {
      alert("Email must end with @telkom.co.id");
    } else if (isDataExists){
    alert("Name and email already exist.")
    } else {
    e.preventDefault();
      const formData = {name,email};
      updateFormData(formData);
      setIsAuthenticated(true); 
      setIsNextClicked(true); 
      navigate("/dashboard");
    }
  };

  


  const checkDataExists = async (name, email) => {
    try {
      const response = await fetch(`http://localhost:5000/users?name=${name}&email=${email}`);
      const data = await response.json()
      return data.length > 0;
    } catch (error) {
      console.error("Error checking data:", error);
      return false;
    }
  };

  return (
    <div
      className="bg-white"
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        minHeight: "100vh",
      }}
    >
      <div>
        <p className="flex justify-center text-3xl mb-10">
          Welcome to Our LLM Page ! {"\u{1F44B}"}
        </p>
        <div
          className="rounded-lg border-red-700"
          style={{ borderWidth: "2px" }}
        >
          <div className="ml-20 mr-20 mt-10 mb-10">
            <form className="space-y md:space-y-6">
              <div>
                <label className="block mb-2 font-medium text-gray-900 dark:text-black">
                  Name
                </label>
                <input
                  type="text"
                  name="name"
                  value={name}
                  onChange={(e) => setName(e.target.value)}
                  className="input-style"
                  placeholder="Enter Your Name"
                />
              </div>
              <div>
                <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-black">
                  Telkom E-mail
                </label>
                <input
                  type="text"
                  name="email"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                  className="input-style"
                  placeholder="Enter Your Telkom Email"
                />
              </div>
              <div className="flex mr-10">
                <label
                  htmlFor="phone"
                  className="block mb-2 mr-5 text-sm font-medium text-gray-900 dark:text-black"
                >
                  Is the given data already appropriate?
                </label>
                <div className="flex items-center">
                  <input
                    type="checkbox"
                    className="checkbox-style"
                    onChange={handleCheckboxChange}
                    required
                  />
                  <label
                    htmlFor="terms1"
                    className="block ml-1 text-sm font-medium text-gray-900 dark:text-black"
                  >
                    Yes
                  </label>
                </div>
              </div>
            </form>
          </div>
        </div>
        {alertMessage && <p className="text-red-500 mt-2">{alertMessage}</p>}
        <div>
          <button
            type="submit"
            className={`button-style ${isChecked ? "" : "disabled"}`}
            onClick={handleNextButtonClick}
          >
            Next <span>&gt;</span>
          </button>
        </div>
      </div>
    </div>
  );
}

export default Form;
