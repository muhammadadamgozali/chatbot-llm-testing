/* eslint-disable react-refresh/only-export-components */
import { createContext, useContext, useState } from 'react';
import PropTypes from 'prop-types';

const AppContext = createContext();
const NextButtonContext = createContext(); 

export const AppProvider = ({ children }) => {
  const [formData, setFormData] = useState({});
  const [isNextClicked, setIsNextClicked] = useState(false); 
  const [isAuthenticated, setIsAuthenticated] = useState(false); 

  const updateFormData = (data) => {
    setFormData(data);
  };

  return (
    <AppContext.Provider value={{ formData, updateFormData }}>
      <NextButtonContext.Provider value={{ isNextClicked, setIsNextClicked, isAuthenticated, setIsAuthenticated }}> 
        {children}
      </NextButtonContext.Provider>
    </AppContext.Provider>
  );
};

// Tambahkan prop types
AppProvider.propTypes = {
  children: PropTypes.node.isRequired
};

export const useAppContext = () => {
  return useContext(AppContext);
};

export const useNextButtonContext = () => {
  return useContext(NextButtonContext);
};
