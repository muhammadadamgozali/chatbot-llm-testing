import PropTypes from "prop-types";
import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAngleRight, faTimes } from "@fortawesome/free-solid-svg-icons";
import "./Sidebar.css";
import { useNextButtonContext } from "../context/Context";

function Sidebar({ counter, isOpen, toggleSidebar, togglePopup }) {
  const [activeRoute, setActiveRoute] = useState("home");
  const { isNextClicked } = useNextButtonContext();
  const [isSubmitActive, setIsSubmitActive] = useState(false);

  useEffect(() => {
    if (counter >= 10) {
      setIsSubmitActive(true);
    } else {
      setIsSubmitActive(false);
    }
  }, [counter]);

  useEffect(() => {
    if (isNextClicked) {
      setActiveRoute("dashboard");
    }
  }, [isNextClicked]);

  const handleRouteClick = (route) => {
    setActiveRoute(route);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    togglePopup();
  };

  return (
    <div>
      <div className="Sidebar-btn" onClick={toggleSidebar}>
        <FontAwesomeIcon icon={faAngleRight} />
      </div>
      <aside className={`sidebar ${isOpen ? "open" : ""}`}>
        <button className="toggle-btn" onClick={toggleSidebar}>
          <FontAwesomeIcon icon={faTimes} />
        </button>
        <div className="sidebar-content mt-20">
          <span>
            <Link
              // to="/"
              onClick={() => handleRouteClick("home")}
              className={activeRoute === "home" ? "btn-sidebar" : ""}
              style={{
                color: activeRoute === "home" ? "" : "rgba(166, 33, 16, 1)",
                marginLeft: "10px",
                fontFamily: "poppins",
                pointerEvents: isNextClicked ? "none" : "auto", 
              }}
            >
              Welcome
            </Link>
            <br></br>
            <br></br>
            <Link
              // to="/dashboard"
              onClick={() => handleRouteClick("dashboard")}
              className={` ${!isNextClicked ? "" : "btn-sidebar2 "}`}
              style={{
                color:
                  activeRoute === "dashboard" ? "" : "rgba(166, 33, 16, 1)",
                marginLeft: "10px",
                marginTop: "10px",
                fontFamily: "poppins",
                pointerEvents: isNextClicked ? "auto" : "none",
              }}
              disabled={!isNextClicked}
            >
              Choose Your Side
            </Link>
          </span>
          {isNextClicked && (
            <div>
              <div
                className="flex"
                style={{ marginTop: "30rem", fontSize: "11px" }}
              >
                <div
                  className="sidebar-fill bg-white rounded-lg"
                  style={{
                    background: `linear-gradient(to right, rgba(166, 33, 16, 1) ${
                      counter * 10
                    }%, white ${counter * 10}%)`,
                    borderRadius: "100px",
                    marginRight: "2px",
                  }}
                ></div>
                <p style={{ color: "red", marginRight: "2px" }}>
                  {counter}/10
                </p>
                <p>Messages</p>
              </div>

              <form onSubmit={handleSubmit}>
                <button
                  type="submit"
                  className="submit-button mt-4"
                  style={{
                    background: isSubmitActive ? "rgba(166, 33, 16, 1)" : "rgba(166, 33, 16, 0.8)",
                    padding: "6px",
                    color: "white",
                    borderRadius: "5px",
                    width: "100px",
                    cursor: isSubmitActive ? "pointer" : "not-allowed",
                    fontFamily: "Poppins",
                    textAlign: "center",
                    marginLeft: "28%",
                  }}
                  disabled={!isSubmitActive}
                >
                  Submit
                </button>
              </form>
            </div>
          )}
        </div>
      </aside>
    </div>
  );
}

Sidebar.propTypes = {
  counter: PropTypes.number.isRequired,
  isOpen: PropTypes.bool.isRequired,
  toggleSidebar: PropTypes.func.isRequired,
  togglePopup: PropTypes.func.isRequired,
};

export default Sidebar;
