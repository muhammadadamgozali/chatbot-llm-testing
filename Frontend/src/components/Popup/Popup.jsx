import { useState, useEffect } from "react";
import { useAppContext } from '../context/Context';
import PropTypes from "prop-types";
import "./Popup.css";

const Popup = ({ onClose }) => {
  const [showSuccess, setShowSuccess] = useState(false);
  const [showMainPopup, setShowMainPopup] = useState(true);
  const [showCheck, setShowCheck] = useState(false);
  const { formData } = useAppContext();

  useEffect(() => {
    const timer = setTimeout(() => {
      setShowCheck(true);
    }, 1000);

    return () => clearTimeout(timer);
  }, []);

  const handleYesSubmit = async () => {
    setShowMainPopup(false);
    try {
      const response = await fetch('http://localhost:5000/users', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(formData),
      });
      if (response.ok) {
        console.log('Data successfully submitted to server');
      } else {
        console.error('Failed to submit data to server');
      }
    } catch (error) {
      console.error('Error submitting data to server:', error.message);
    }
    setShowSuccess(true);
  };

  const handleClose = () => {
    setTimeout(() => {
      setShowSuccess(false);
      onClose();
      window.location.href = '/';
    });
  };

  return (
    <div className="popup-overlay">
      {showMainPopup && (
        <div className="popup-content">
          <p>Are you sure want to submit this process?</p>
          <button onClick={handleYesSubmit}>Yes, Submit</button>
          <button
            style={{
              background: "rgba(256, 222, 218, 1)",
              color: "rgba(166, 33, 16, 1)",
            }}
            onClick={onClose}
          >
            Cancel
          </button>
        </div>
      )}
      {showSuccess && (
        <div className="popup-content">
          <div
            className="img-container"
            style={{
              zIndex: "999",
              width: "70px",
              height: "auto",
              marginLeft: "36px",
              marginBottom: "80px",
            }}
          >
            {showCheck && (
              <img
                className="animated-check"
                src="/centang.svg"
                alt="Centang"
              />
            )}
          </div>
          <a
            style={{
              fontWeight: "bold",
              fontSize: "24px",
              color: "rgba(166, 33, 16, 1)",
              marginLeft: "10px",
            }}
          >
            Thank you!
          </a>
          <p>Submission succesful</p>
          <button
            onClick={handleClose}
            style={{
              marginLeft: "3rem",
            }}
          >
            OK
          </button>
        </div>
      )}
    </div>
  );
};

Popup.propTypes = {
  onClose: PropTypes.func.isRequired,
};

export default Popup;
