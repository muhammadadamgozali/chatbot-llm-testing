
import PropTypes from "prop-types";
import "./PopupChat.css";

const PopupChat = ({onClose }) => {
  return (
    <div className="popup">
      <div className="popup-inner">
        <p>Select most prefer response and submit!</p>
        <button className="button-close" onClick={onClose}>Close</button>
      </div>
    </div>
  );
};

PopupChat.propTypes = {
  message: PropTypes.string.isRequired,
  onClose: PropTypes.func.isRequired,
};

export default PopupChat;
