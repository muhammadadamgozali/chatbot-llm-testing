import { useState, useRef, useEffect } from "react";
import { Navigate } from "react-router-dom";
import PropTypes from 'prop-types';
import axios from "axios";
import "./Chat.css";
import PopupChat from "../Popup/PopupChat"; 
import { useAppContext, useNextButtonContext } from '../context/Context';

// Load environment variables
const apiUrl = import.meta.env.VITE_API_URL;
const apiKey = import.meta.env.VITE_API_KEY;

const Chat = ({ onMessageSubmit }) => {
  const [input1, setInput1] = useState("");
  const [output1, setOutput1] = useState("");
  const [output2, setOutput2] = useState("");
  const [isInputFilled, setIsInputFilled] = useState(false);
  const [selectedCards, setSelectedCards] = useState(Array(10).fill(null)); 
  const [questions, setQuestions] = useState([]);
  const [submittedQuestions, setSubmittedQuestions] = useState([]);
  const [responses, setResponses] = useState([
    { id: "card1", text: "Response 1", messages: [] },
    { id: "card2", text: "Response 2", messages: [] },
  ]);
  const [showPopup, setShowPopup] = useState(false);
  const [popupMessage] = useState("");
  const messagesEndRef = useRef(null);
  const { formData, updateFormData } = useAppContext();
  const { isAuthenticated } = useNextButtonContext();
  const [cardClickCounts, setCardClickCounts] = useState({});

  useEffect(() => {
    scrollToBottom();
  }, [responses]);

  if (!isAuthenticated) {
    return <Navigate to="/" />; 
  }

  const handleClick = (id, index) => { 
    setSelectedCards((prevSelectedCards) =>
      prevSelectedCards.map((prevSelectedCard, i) =>
        i === index ? id : prevSelectedCard 
      )
    );
    if (!submittedQuestions.includes(index) && (id === "card1" || id === "card2")) {
      const newCount = (cardClickCounts[id] || 0) + 1;
      setCardClickCounts({ ...cardClickCounts, [id]: newCount });
      if (id === 'card1') {
        updateFormData({
          ...formData,
          left: newCount,
        });
      } else if (id === 'card2') {
        updateFormData({
          ...formData,
          right: newCount,
        });
      }
      onMessageSubmit();
      setSubmittedQuestions([...submittedQuestions, index]);
    }
  };

  const handleResponse = async (response, id) => { 
    const message =
      response.data.choices[0]?.message?.content || "Not found";
    setResponses((prevResponses) =>
      prevResponses.map((prevResponse) =>
        prevResponse.id === id
          ? { ...prevResponse, messages: [...prevResponse.messages, message] }
          : prevResponse
      )
    );
  };

  const handleSubmit = async (e) => { 
    e.preventDefault();
    const userInput = input1.trim();
    if (questions.length >= 10) {
      setShowPopup(true);
      return;
    }
    setQuestions([ ...questions,userInput]);
    setInput1("");
    try {
      const response1 = await axios.post(
        apiUrl,
        {
          messages: [
            {
              role: "system",
              content: "You are an AI assistant that helps people find information.",
            },
            { role: "user", content: userInput },
          ],
          max_tokens: 800,
          temperature: 0.7,
          frequency_penalty: 0,
          presence_penalty: 0,
          top_p: 0.95,
          stop: null,
        },
        {
          headers: {
            "Content-Type": "application/json",
            "api-key": apiKey,
          },
        }
      );

      setOutput1(response1.data.choices[0]?.message?.content || "Not found");
      handleResponse(response1, "card1"); 
      console.log("Output 1:", output1);
      const response2 = await axios.post(
        apiUrl,
        {
          messages: [
            {
              role: "system",
              content: "You are an AI assistant that helps people find information.",
            },
            { role: "user", content: userInput },
          ],
          max_tokens: 800,
          temperature: 0.7,
          frequency_penalty: 0,
          presence_penalty: 0,
          top_p: 0.95,
          stop: null,
        },
        {
          headers: {
            "Content-Type": "application/json",
            "api-key": apiKey,
          },
        }
      );

      setOutput2(response2.data.choices[0]?.message?.content || "Not found");
      handleResponse(response2, "card2");
      console.log("Output 2:", output2);
    } catch (error) {
      console.error("Error occurred while sending data:", error);
      setOutput1("Not found");
      setOutput2("Not found");
    }
  };

  const scrollToBottom = () => {
    messagesEndRef.current?.scrollIntoView({ behavior: 'smooth' });
  };

  const handleChange = (e) => {
    setInput1(e.target.value);
    setIsInputFilled(e.target.value.trim() !== "");
  };

  const closePopup = () => {
    setShowPopup(false);
  };

  return (
    <div className="ml-10 mr-10 bg-white-100 h-screen flex flex-col dark:bg-white-100 ">
      <div className="flex-1 overflow-y-scroll">
        {questions.length > 0 ? (
          questions.map((question, index) => (
            <div key={index}>
              <div className="card mt-20 border-red-600">
                <div className="card-header">{"\u{1F534}"} You</div>
                <div className="card-body ml-7">{question}</div>
              </div>
              <div
                className="mt-12"
                style={{ fontFamily: "Poppins, sans-serif" }}
              >
                <h5 style={{ textAlign: "center", fontWeight: "bold" }}>
                  Which response do you prefer?
                </h5>
                <p className="text-gray-400 mb-8 mt-2 text-center">
                  You can click on one of the response options below.
                </p>
                <div style={{ display: "flex", marginBottom: "5px" }}>
                  {responses.map((response, i) => ( 
                    <div
                      key={i}
                      className={`card2 w-full ${
                        selectedCards[index] === response.id ? "clicked" : ""
                      }`}
                      style={{ marginRight: "1rem" }}
                      onClick={() => handleClick(response.id, index)}
                    >
                      <div className="card-header flex">
                        <img
                          style={{ width: "20px", marginRight: "8px" }}
                          src="/lingkaranCard.svg"
                        />{" "}
                        {response.text}
                      </div>
                      <div className="card-body mt-3 ml-2">
                        {response.messages[index] ? (
                          <p>{response.messages[index]}</p>
                        ) : (
                          <p className="mt-3 ml-2">Content goes...</p>
                        )}
                      </div>
                    </div>
                  ))}
                </div>
              </div>
            </div>
          ))
        ) : (
          <div className="card border-red-800 ml-20 mr-20">
            <div className="card-header">No questions yet</div>
            <div className="card-body">
              We are available to answer your questions. Please provide the
              information you need.
            </div>
          </div>
        )}
         <div ref={messagesEndRef} />
      </div>
      <form onSubmit={handleSubmit} className="relative">
        <div className="flex items-center mb-10 mt-5 border rounded-lg border-red-700">
          <input
            type="text"
            name="kirim"
            className="flex-1 rounded-lg py-2 px-4"
            value={input1}
            onChange={handleChange}
            placeholder="Write questions or messages here.."
          />
          <button
            type="submit"
            className={!isInputFilled ? "red" : "blue"}
            style={{ position: "absolute", right: "10px", cursor: "pointer" }}
            disabled={input1.trim() === ""}
          >
            <img src="/send.svg" alt="Send" />
          </button>
        </div>
      </form>
      {showPopup && <PopupChat message={popupMessage} onClose={closePopup} />} 
    </div>
  );
};

Chat.propTypes = {
  onMessageSubmit: PropTypes.func.isRequired, 
};
export default Chat;
