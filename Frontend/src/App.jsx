// App.js
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import { useState } from "react";
// import Header from './components/Navbar/Navbar';
import Sidebar from "./components/Sidebar/Sidebar";
import Home from "./pages/Home/Home";
import Dashboard from "./pages/Dashboard/Dashboard";
import Popup from "./components/Popup/Popup";
import { AppProvider } from '../src/components/context/Context';

import "./App.css";

const App = () => {
  const [isOpen, setIsOpen] = useState(true);
  const [showPopup, setShowPopup] = useState(false);
  const [counter, setCounter] = useState(0);
  

  const incrementCounter = () => {
    setCounter(counter + 1);
  };

  const toggleSidebar = () => {
    setIsOpen(!isOpen);
  };

  const togglePopup = () => {
    setShowPopup(!showPopup);
  };

  const closePopup = () => {
    setShowPopup(false);
  };



  return (
    <AppProvider>
    <div>
      <Router>
        {/* <Header/> */}
        <div className="app">
          <Sidebar
          counter={counter} 
            path="/Sidebar"
            isOpen={!isOpen}
            toggleSidebar={toggleSidebar}
            togglePopup={togglePopup}
            component={Home}
          />
          <div className={`content ${isOpen ? "" : "content-sidebar-closed"}`}>
            <Routes>
              <Route exact path="/" element={<Home />} />
              <Route exact path="/dashboard" element={<Dashboard onMessageSubmit={incrementCounter}/>} />
            </Routes>
          </div>
        </div>
      </Router>
      {showPopup && <Popup onClose={closePopup} />}
    </div>
    </AppProvider>
  );
};

export default App;
