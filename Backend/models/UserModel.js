import mongoose from 'mongoose';

const User = mongoose.Schema({
    name: {
        type: String,
        required: false
    },
    email: {
        type: String,
        required: false
    },
    right: {
        type: Number,
        default: null,
       
        
    },
    left: {
        type: Number,
        default: null,
      
    }
});

export default mongoose.model('Users', User);