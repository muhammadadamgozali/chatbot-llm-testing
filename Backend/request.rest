GET http://localhost:5000/users

###
GET http://localhost:5000/users/6622151e2e65dbae7a359bba

###
POST  http://localhost:5000/users
content-Type: application/json

{
    "name" : "Muhammad Adam Gozali",
    "email" : "adam@telkom.co.id",
    "right" : 10,
    "left" : 10
}

###
PATCH  http://localhost:5000/users/66216cccb5ec49c6bbf091ff
content-Type: application/json

{
    "name" : "amang",
    "email" : "amang@telkom.co.id",
    "right" : 3,
    "left" : 1
}

###
DELETE   http://localhost:5000/users/6623bf07c7a4290ef272bb90
content-Type: application/json